package cn.smile.commons.web;

import cn.hutool.core.util.StrUtil;
import cn.smile.commons.exceptions.BusinessException;
import cn.smile.commons.response.ResponseCode;

/**
 * <p>
 * 请求头处理
 * </>
 *
 * @author 龙逸
 * @since: 2020-10-28 14:04:11
 **/
public class Header {
    private static final String AUTHORIZATION_BEARER_TOKEN = "Bearer";

    /**
     * 获取 Token
     *
     * @param header {@code String} request.getHeader("Authorization")
     * @return {@code String} token
     */
    public static String getAuthorization(String header) {
        if (StrUtil.isBlank(header) || !header.startsWith(AUTHORIZATION_BEARER_TOKEN)) {
            throw new BusinessException(ResponseCode.USER_NOT_LOGGED_IN);
        }
        return header.substring(AUTHORIZATION_BEARER_TOKEN.length() + 1);
    }
}
