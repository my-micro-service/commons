package cn.smile.commons.enums;

/**
 * <p>
 * 音频格式枚举类
 * </>
 *
 * @author 龙逸
 * @since: 2020-12-20 18:44:27
 **/
public enum VoiceFormatEnum {
    //PCM 格式
    PCM(1, "pcm"),
    //WAV 格式
    WAV(2, "wav"),
    //MP3 格式
    MP3(3, "mp3");

    VoiceFormatEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    private Integer code;
    private String text;

    public Integer getCode() {
        return code;
    }

    public String getText() {
        return text;
    }

    public static VoiceFormatEnum get(Integer code) {
        for (VoiceFormatEnum voiceFormatEnum : VoiceFormatEnum.values()) {
            if (voiceFormatEnum.code.equals(code)) {
                return voiceFormatEnum;
            }
        }
        return null;
    }

    public static String getDescByCode(Integer code) {
        VoiceFormatEnum voiceFormatEnum = get(code);
        if (voiceFormatEnum == null) {
            return null;
        }
        return voiceFormatEnum.getText();
    }
}
