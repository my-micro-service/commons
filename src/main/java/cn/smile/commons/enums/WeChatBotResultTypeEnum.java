package cn.smile.commons.enums;

/**
 * <p>
 * 微信公众号返回消息类型
 * </p>
 *
 * @author longjuntao
 * @since 2020/12/21 10:20
 */
public enum WeChatBotResultTypeEnum {
    //回复消息类型
    TEXT(1, "文本消息"),
    VOICE(2, "语音消息");

    WeChatBotResultTypeEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    private Integer code;
    private String text;

    public Integer getCode() {
        return code;
    }

    public String getText() {
        return text;
    }

    public static WeChatBotResultTypeEnum get(Integer code) {
        for (WeChatBotResultTypeEnum typeEnum : WeChatBotResultTypeEnum.values()) {
            if (typeEnum.code.equals(code)) {
                return typeEnum;
            }
        }
        return null;
    }

    public static String getDescByCode(Integer code) {
        WeChatBotResultTypeEnum typeEnum = get(code);
        if (typeEnum == null) {
            return null;
        }
        return typeEnum.getText();
    }
}
