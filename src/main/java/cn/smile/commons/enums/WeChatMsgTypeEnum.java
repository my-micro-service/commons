package cn.smile.commons.enums;

/**
 * <p>
 * 微信消息类型
 * </p>
 *
 * @author longjuntao
 * @since 2020/10/30 16:55
 */
public enum WeChatMsgTypeEnum {
    //枚举类型
    TEXT("text", "文本消息"),
    EVENT("event", "关注/取消关注"),
    IMAGE("image", "图片消息"),
    VOICE("voice", "语音消息"),
    VIDEO("video", "视频消息"),
    SHORT_VIDEO("shortvideo", "小视频消息"),
    LOCATION("location", "地理位置消息"),
    LINK("link", "链接消息");

    WeChatMsgTypeEnum(String code, String text) {
        this.code = code;
        this.text = text;
    }

    private String code;
    private String text;

    public String getCode() {
        return code;
    }

    public String getText() {
        return text;
    }

    public static WeChatMsgTypeEnum get(String code) {
        for (WeChatMsgTypeEnum weChatMsgTypeEnum : WeChatMsgTypeEnum.values()) {
            if (weChatMsgTypeEnum.code.equals(code)) {
                return weChatMsgTypeEnum;
            }
        }
        return null;
    }

    public static String getDescByCode(String code) {
        WeChatMsgTypeEnum weChatMsgTypeEnum = get(code);
        if (weChatMsgTypeEnum == null) {
            return null;
        }
        return weChatMsgTypeEnum.getText();
    }
}
