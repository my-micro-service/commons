package cn.smile.commons.enums;

/**
 * <p>
 *
 * </p>
 *
 * @author 龙逸
 * @since 2021-06-29
 */
public enum SourceEnum {
    ADMIN("admin"), FRONT("front");

    private String sn;

    SourceEnum(String sn) {
        this.sn = sn;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }
}
