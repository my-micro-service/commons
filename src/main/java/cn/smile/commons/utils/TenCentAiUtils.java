package cn.smile.commons.utils;

import cn.smile.commons.constant.CommonConstant;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * <p>
 * 腾讯AI接口
 * </p>
 *
 * @author longjuntao
 * @since 2020/12/18 16:34
 */
public class TenCentAiUtils {

    private static final Logger logger = LoggerFactory.getLogger(TenCentAiUtils.class);

    /**
     * 请求腾讯AI接口
     *
     * @param restTemplate restTemplate对象
     * @param signParams   请求参数
     * @return API接口返回结果
     */
    public static String requestToUrl(RestTemplate restTemplate, Map<String, String> signParams, MultiValueMap<String, Object> params, String url) {

        //应用标识（AppId）
        signParams.put(CommonConstant.CHAT_BOT_APP_ID, CommonConstant.CHAT_BOT_APP_ID_VALUE);
        //请求时间戳（秒级）
        signParams.put(CommonConstant.MAP_KEY_TIME_STAMP, String.valueOf(System.currentTimeMillis() / 1000));
        //随机字符串
        signParams.put(CommonConstant.MAP_KEY_NONCE_STR, RandomUtils.generateString(16));

        try {
            params.add(CommonConstant.MAP_KEY_SIGN, getSignString(signParams));
        } catch (IOException e) {
            logger.error("[TenCentAiUtils].[requestToUrl]------> getSignString error :", e);
        }
        //应用标识（AppId）
        params.add(CommonConstant.CHAT_BOT_APP_ID, Long.valueOf(CommonConstant.CHAT_BOT_APP_ID_VALUE));
        //请求时间戳（秒级）
        params.add(CommonConstant.MAP_KEY_TIME_STAMP, Long.valueOf(signParams.get(CommonConstant.MAP_KEY_TIME_STAMP)));
        //随机字符串
        params.add(CommonConstant.MAP_KEY_NONCE_STR, signParams.get(CommonConstant.MAP_KEY_NONCE_STR));

        //组装Headers及请求体
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<MultiValueMap<String, Object>> formEntity = new HttpEntity<>(params, headers);

        logger.info("[TenCentAiUtils].[requestToUrl] formEntity = {}", JSON.toJSONString(formEntity));

        //组装请求URL
        ResponseEntity<String> resultEntity = restTemplate.postForEntity(url, formEntity, String.class);
        if (CommonConstant.HTTP_REQUEST_SUCCESS_CODE != resultEntity.getStatusCodeValue()) {
            logger.error("[TenCentAiUtils].[requestToUrl]------> Failed to send WeChat bot");
            return null;
        } else {
            logger.info("[TenCentAiUtils].[requestToUrl]------> resultEntity.getBody() = {}", resultEntity.getBody());

            return resultEntity.getBody();
        }
    }

    /**
     * 获取加密后的sign字符串
     *
     * @param params 请求参数对象
     */
    private static String getSignString(Map<String, String> params) throws IOException {

        StringBuilder stringBuilder = new StringBuilder();

        Map<String, String> sortedParams = new TreeMap<>(params);
        Set<Map.Entry<String, String>> entryList = sortedParams.entrySet();

        for (Map.Entry<String, String> entry : entryList) {
            if (!StringUtils.isEmpty(entry.getValue()) && !StringUtils.isEmpty(entry.getKey().trim()) && !CommonConstant.MAP_KEY_SIGN.equals(entry.getKey())) {
                stringBuilder.append(entry.getKey().trim()).append(CommonConstant.EQUAL)
                        .append(URLEncoder.encode(entry.getValue(), CommonConstant.URL_ENCODE)).append(CommonConstant.GET_URL_AND);
            }
        }

        if (stringBuilder.length() > 0) {
            stringBuilder.deleteCharAt(stringBuilder.length() - 1)
                    .append(String.format("%s%s%s", CommonConstant.GET_URL_AND, CommonConstant.CHAT_BOT_APP_KEY, CommonConstant.EQUAL)).append(CommonConstant.CHAT_BOT_APP_KEY_VALUE);
        }

        logger.info("[TenCentAiUtils].[getSignString] ------> stringBuilder = {}", stringBuilder.toString());

        return Md5Util.getMd5(stringBuilder.toString());
    }
}
