package cn.smile.commons.utils;

import cn.smile.commons.constant.CommonConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * <p>
 * 微信素材上传
 * </p>
 *
 * @author longjuntao
 * @since 2020/12/21 11:26
 */
public class WeChatUploadUtil {

    private static final Logger logger = LoggerFactory.getLogger(WeChatUploadUtil.class);

    /**
     * 微信公众号上传素材
     *
     * @param restTemplate restTemplate连接Bean
     * @param path         文件路径
     * @param url          请求的Url
     * @return 上传结果
     */
    public static String uploadPermanentMedia(RestTemplate restTemplate, String path, String url, String fileType) {

        url = String.format("%s%s%s%s%s%s%s%s", url, CommonConstant.ACCESS_TOKEN_KEY, CommonConstant.EQUAL, RedisUtil.getBucket(CommonConstant.WE_CHAT_TOKEN_KEY).get(),
                CommonConstant.GET_URL_AND, CommonConstant.TYPE_KEY, CommonConstant.EQUAL, fileType);
        try {
            //读取文件
            FileSystemResource fileSystemResource = new FileSystemResource(path);

            MultiValueMap<String, Object> params = new LinkedMultiValueMap<>();
            params.add(CommonConstant.UPLOAD_PARAM_FILE_KEY, fileSystemResource);
            params.add(CommonConstant.UPLOAD_PARAM_FILE_NAME_KEY, fileSystemResource.getFilename());
            params.add(CommonConstant.UPLOAD_PARAM_FILE_LENGTH_KEY, String.valueOf(fileSystemResource.contentLength()));

            //组装Headers及请求体
            HttpHeaders headers = new HttpHeaders();
            MediaType type = MediaType.parseMediaType("multipart/form-data");
            headers.setContentType(type);
            HttpEntity<MultiValueMap<String, Object>> formEntity = new HttpEntity<>(params, headers);

            ResponseEntity<String> resultEntity = restTemplate.postForEntity(url, formEntity, String.class);
            if (CommonConstant.HTTP_REQUEST_SUCCESS_CODE != resultEntity.getStatusCodeValue()) {
                logger.error("[WeChatMessageUtil].[sendWeChatMessageToUserName]------> Failed to reply to WeChat message");
            } else {
                logger.info("[WeChatMessageUtil].[sendWeChatMessageToUserName]------> resultEntity.getBody() = {}", resultEntity.getBody());
                return resultEntity.getBody();
            }
        } catch (Exception e) {
            logger.error("[WeChatUploadUtil].[uploadPermanentMedia] ------> Load File Error :", e);
        }

        return null;
    }
}
