package cn.smile.commons.utils;

import cn.smile.commons.constant.CommonConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * <p>
 * NowApi接口工具类
 * </p>
 *
 * @author longjuntao
 * @since 2020/11/24 10:44
 */
public class NowApiUtil {

    private static final Logger logger = LoggerFactory.getLogger(NowApiUtil.class);

    public static String sendGetRequest(RestTemplate restTemplate, String url) {

        ResponseEntity<String> resultEntity = restTemplate.getForEntity(url, String.class);

        if (CommonConstant.HTTP_REQUEST_SUCCESS_CODE != resultEntity.getStatusCodeValue()) {
            logger.error("[NowApiUtil].[sendGetRequest]------> Failed to request NowApi");
            return null;
        } else {
            logger.info("[NowApiUtil].[sendGetRequest]------> resultEntity.getBody() = {}", resultEntity.getBody());
            return resultEntity.getBody();
        }
    }
}
