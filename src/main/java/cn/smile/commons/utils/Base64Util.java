package cn.smile.commons.utils;

import cn.smile.commons.bean.dto.voice.VoiceCompoundDTO;
import cn.smile.commons.enums.VoiceFormatEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.BASE64Decoder;

import java.io.File;
import java.io.FileOutputStream;
import java.util.UUID;

/**
 * <p>
 * Base64帮助类
 * </>
 *
 * @author 龙逸
 * @since: 2020-12-20 18:50:44
 **/
public class Base64Util {

    private static final Logger logger = LoggerFactory.getLogger(Base64Util.class);

    /**
     * Base64转音频
     *
     * @param path 存储路近
     * @param dto  音频信息对象
     * @return 存储路近
     */
    public static String base64ToVoice(String path, VoiceCompoundDTO dto) {

        logger.info("[Base64Util].[base64ToVoice] ------> Base64 To Voice, path = {}, dto = {}", path, JSON.toJSONString(dto));


        FileOutputStream out = null;
        String uuid = getUuid();
        try {
            byte[] buffer = new BASE64Decoder().decodeBuffer(dto.getSpeech());
            out = new FileOutputStream(path + File.separator + uuid + "." + VoiceFormatEnum.getDescByCode(dto.getFormat()));
            out.write(buffer);
            out.close();
        } catch (Exception e) {
            logger.error("[Base64Util].[base64ToVoice] ------> error :", e);
        }

        return String.format("%s%s%s%s%s", path, File.separator, uuid, ".", VoiceFormatEnum.getDescByCode(dto.getFormat()));
    }

    /**
     * 获取UUID
     *
     * @return UUID
     */
    private static String getUuid() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
