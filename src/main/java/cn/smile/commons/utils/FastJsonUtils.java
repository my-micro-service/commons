package cn.smile.commons.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @author smile-jt
 * @Created 2021/6/28 17:11
 */
@SuppressWarnings("unused")
public class FastJsonUtils {

    /**
     * JSON 转List
     * @param arrayJson JSON字符串
     * @param clazz List对象
     * @param <T> 泛型
     * @return 转换结果
     */
    public static <T> List<T> jsonToList(String arrayJson, Class<T> clazz) {
        Assert.notNull(arrayJson, "Json数组不能为空!");
        return JSON.parseArray(arrayJson, clazz);
    }

    /**
     * 列表转化成Json数组
     *
     * @param list List列表数据
     * @return JSON数组
     */
    public static <T> String listToJson(List<T> list) {
        String resultStr = null;
        if (!CollectionUtils.isEmpty(list)) {
            JSONArray array = JSON.parseArray(JSON.toJSONString(list));
            resultStr = array.toJSONString();
        }
        return resultStr;
    }

    /**
     * 对象转成json
     *
     * @param t 需要转换的对象
     * @return 转换结果
     */
    public static <T> String objectToJson(T t) {
        Assert.notNull(t, "对象不能为空!");
        return JSON.toJSONString(t, SerializerFeature.DisableCircularReferenceDetect);
    }

    public static <T> String objectToJson(T t, SerializerFeature... features) {
        Assert.notNull(t, "对象不能为空!");
        return JSON.toJSONString(t, features);
    }

    /**
     * json转成对象
     *
     * @param json JSON字符串
     * @param t 需要转换的对象class
     * @return 转换结果
     */
    public static <T> T jsonToObject(String json, Class<T> t) {
        Assert.notNull(json, "json字符串不能为空!");
        return JSON.parseObject(json, t);
    }
}
