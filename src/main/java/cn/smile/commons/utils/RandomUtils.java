package cn.smile.commons.utils;

import java.util.Random;

/**
 * <p>
 *
 * </p>
 *
 * @author longjuntao
 * @since 2020/11/30 11:02
 */
@SuppressWarnings("unused")
public class RandomUtils {
    public static final String ALL_CHAR = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String LETTER_CHAR = "abcdefghijkllmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String NUMBER_CHAR = "0123456789";
    public static final int[] NUMBER_CHAR_ARR = { 1, 2, 3, 4, 5, 6, 7 };

    /**
     * 返回一个定长的随机字符串(只包含大小写字母、数字)
     *
     * @param length 随机字符串长度
     * @return 随机字符串
     */
    public static String generateString(int length) {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            sb.append(ALL_CHAR.charAt(random.nextInt(ALL_CHAR.length())));
        }
        return sb.toString();
    }

    /**
     * 返回一个定长的随机纯字母字符串(只包含大小写字母)
     *
     * @param length 随机字符串长度
     * @return 随机字符串
     */
    public static String generateMixString(int length) {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            sb.append(ALL_CHAR.charAt(random.nextInt(LETTER_CHAR.length())));
        }
        return sb.toString();
    }

    /**
     * 返回一个定长的随机纯大写字母字符串(只包含大小写字母)
     *
     * @param length 随机字符串长度
     * @return 随机字符串
     */
    public static String generateLowerString(int length) {
        return generateMixString(length).toLowerCase();
    }

    /**
     * 返回一个定长的随机纯小写字母字符串(只包含大小写字母)
     *
     * @param length 随机字符串长度
     * @return 随机字符串
     */
    public static String generateUpperString(int length) {
        return generateMixString(length).toUpperCase();
    }

    /**
     * 生成一个定长的纯0字符串
     *
     * @param length 字符串长度
     * @return 纯0字符串
     */
    public static String generateZeroString(int length) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            sb.append('0');
        }
        return sb.toString();
    }

    /**
     * 根据数字生成一个定长的字符串，长度不够前面补0
     *
     * @param num       数字
     * @param fixLength 字符串长度
     * @return 定长的字符串
     */
    public static String toFixLengthString(long num, int fixLength) {
        StringBuilder sb = new StringBuilder();
        String strNum = String.valueOf(num);
        if (fixLength - strNum.length() >= 0) {
            sb.append(generateZeroString(fixLength - strNum.length()));
        } else {
            throw new RuntimeException("将数字" + num + "转化为长度为" + fixLength + "的字符串发生异常！");
        }
        sb.append(strNum);
        return sb.toString();
    }

    /**
     * 每次生成的len位数都不相同
     *
     * @param len 长度
     * @return 定长的数字
     */
    public static int getNotSimple(int len) {
        Random rand = new Random();
        for (int i = NUMBER_CHAR_ARR.length; i > 1; i--) {
            int index = rand.nextInt(i);
            int tmp = NUMBER_CHAR_ARR[index];
            NUMBER_CHAR_ARR[index] = NUMBER_CHAR_ARR[i - 1];
            NUMBER_CHAR_ARR[i - 1] = tmp;
        }
        int result = 0;
        for (int i = 0; i < len; i++) {
            result = result * 10 + NUMBER_CHAR_ARR[i];
        }
        return result;
    }

}
