package cn.smile.commons.constant;

/**
 * <p>
 * 数字常量
 * </p>
 *
 * @author longjuntao
 * @since 2021/2/24 14:00
 */
public class NumberConstant {

    public static final int ZERO = 0;
    public static final int ONE = 1;
    public static final int TWO = 2;
    public static final int FIVES = 5;
}
