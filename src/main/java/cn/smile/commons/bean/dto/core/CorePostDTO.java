package cn.smile.commons.bean.dto.core;

import cn.smile.commons.bean.domain.core.CorePost;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * </>
 *
 * @author 龙逸
 * @since: 2020-10-28 12:28:22
 **/
@EqualsAndHashCode(callSuper = false)
@Data
@Accessors(chain = true)
public class CorePostDTO extends CorePost {
    private static final long serialVersionUID = 3906377817552287310L;

    /**
     * 文章作者
     */
    private String username;
}
