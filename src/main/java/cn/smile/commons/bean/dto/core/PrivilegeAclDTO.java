package cn.smile.commons.bean.dto.core;

import cn.smile.commons.bean.domain.core.PrivilegeAcl;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.util.StringUtils;

import java.math.BigInteger;
import java.util.Objects;

/**
 * @author smile-jt
 * @Created 2021/6/28 14:01
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class PrivilegeAclDTO extends PrivilegeAcl {
    private static final long serialVersionUID = -7198414203492894328L;

    /**
     * 授权允许
     */
    public static final int ACL_YES = 1;

    /**
     * 授权不允许
     */
    public static final int ACL_NO = 0;

    /**
     * 授权不确定
     */
    public static final int ACL_NEUTRAL = -1;

    private String userNo;

    /**
     * 得到权限
     *
     * @param permission 权限ID
     * @return 结果
     */
    public int getPermission(int permission) {
        if (Objects.isNull(this.getAclState())) {
            this.setAclState(new BigInteger("0"));
        }
        BigInteger temp = new BigInteger("1");
        temp = temp.shiftLeft(permission);
        temp = temp.and(this.getAclState());
        if (!StringUtils.isEmpty(temp.toString()) && !"0".equals(temp.toString())) {
            return ACL_YES;
        }
        return ACL_NO;
    }

    /**
     * 设置权限
     *
     * @param permission 权限ID
     * @param yes ？？？
     */
    public void setPermission(int permission, boolean yes) {
        if (Objects.isNull(this.getAclState())) {
            this.setAclState(new BigInteger("0"));
        }
        BigInteger temp = new BigInteger("1");
        temp = temp.shiftLeft(permission);
        if (yes){
            this.setAclState(this.getAclState().or(temp));
        } else {
            this.setAclState(this.getAclState().xor(temp));
        }
    }
}
