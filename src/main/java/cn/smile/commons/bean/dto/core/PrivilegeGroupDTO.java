package cn.smile.commons.bean.dto.core;

import cn.smile.commons.bean.domain.core.PrivilegeGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

/**
 * @author smile-jt
 * @Created 2021/6/28 15:44
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Slf4j
public class PrivilegeGroupDTO extends PrivilegeGroup {
    private static final long serialVersionUID = -4945770190527988698L;
}
