package cn.smile.commons.bean.dto.core;

import lombok.Data;

import java.io.Serializable;

/**
 * @author smile-jt
 * @Created 2021/6/28 13:50
 */
@Data
public class ModulePermissionDTO implements Serializable {
    private static final long serialVersionUID = 7957646027942606632L;

    /**
     * 模块表示
     */
    private String moduleSn;
    /**
     * 权限名称
     */
    private String permissionName;
    /**
     * 权限位数
     */
    private int permissionValue;
}
