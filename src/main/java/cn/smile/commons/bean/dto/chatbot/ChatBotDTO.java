package cn.smile.commons.bean.dto.chatbot;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author longjuntao
 * @since 2020/11/27 16:48
 */
@Data
public class ChatBotDTO implements Serializable {
    private static final long serialVersionUID = -3868076537817693224L;

    /**
     * 会话标识（应用内唯一）
     */
    private String session;

    /**
     * 用户输入的聊天内容
     */
    private String answer;
}
