package cn.smile.commons.bean.dto.voice;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author longjuntao
 * @since 2020/12/21 17:00
 */
@SuppressWarnings("all")
@Data
public class WeChatMaterialDTO implements Serializable {
    private static final long serialVersionUID = -7521904497492549765L;

    private String type;

    private String media_id;

    private Long created_at;


}
