package cn.smile.commons.bean.dto.voice;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * </>
 *
 * @author 龙逸
 * @since: 2020-12-20 18:39:38
 **/
@Data
public class VoiceCompoundDTO implements Serializable {
    private static final long serialVersionUID = -5434561349070295784L;

    private Integer format;

    private String speech;

    private String md5sum;
}
