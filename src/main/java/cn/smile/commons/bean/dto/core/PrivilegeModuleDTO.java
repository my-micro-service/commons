package cn.smile.commons.bean.dto.core;

import cn.smile.commons.bean.domain.core.PrivilegeModule;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author smile-jt
 * @Created 2021/6/28 14:49
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class PrivilegeModuleDTO extends PrivilegeModule {
    private static final long serialVersionUID = 5970755781556169099L;
}
