package cn.smile.commons.bean.dto.core;

import cn.smile.commons.bean.domain.core.IndexMapping;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author longjuntao
 * @since 2021/2/24 11:51
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class IndexMappingDTO extends IndexMapping {
    private static final long serialVersionUID = 7031696864232699936L;
}
