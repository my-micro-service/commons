package cn.smile.commons.bean.dto.core;

import cn.smile.commons.bean.domain.core.PrivilegeValue;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author smile-jt
 * @Created 2021/6/28 15:02
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class PrivilegeValueDTO extends PrivilegeValue {
    private static final long serialVersionUID = -3371191082909041742L;
}
