package cn.smile.commons.bean.dto.core;

import cn.smile.commons.bean.domain.core.CoreUser;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author smile-jt
 * @Created 2021/6/28 10:34
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CoreUserDTO extends CoreUser {
    private static final long serialVersionUID = 5408372907831387718L;
}
