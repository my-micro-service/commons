package cn.smile.commons.bean.form.dashboard.login;

import lombok.Data;

/**
 * @author smile-jt
 * @Created 2021/6/29 13:49
 */
@Data
public class LoginForm {

    private String username;

    private String password;
}
