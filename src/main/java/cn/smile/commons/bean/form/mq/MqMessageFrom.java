package cn.smile.commons.bean.form.mq;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author longjuntao
 * @since 2020/12/1 16:21
 */
@Data
public class MqMessageFrom implements Serializable {
    private static final long serialVersionUID = 8255334902754109454L;

    private String queueName;

    private String context;
}
