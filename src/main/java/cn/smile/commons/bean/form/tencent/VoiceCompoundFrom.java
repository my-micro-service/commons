package cn.smile.commons.bean.form.tencent;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * </>
 *
 * @author 龙逸
 * @since: 2020-12-20 17:19:43
 **/
@Data
public class VoiceCompoundFrom implements Serializable {
    private static final long serialVersionUID = -2692580320070771998L;

    /**
     * 发声人编码(1-普通话男声；5-静琪女声；6-欢馨女声；7-碧萱女声)
     */
    @NotNull(message = "发声人不能为空")
    @Min(value = 1, message = "发声人编码不合法")
    @Max(value = 7, message = "发声人编码不合法")
    private Integer speakerCode;

    /**
     * 合成语音格式编码(1-PCM；2-WAV；3-MP3)
     */
    @NotNull(message = "合成音频格式不能为空")
    @Min(value = 1, message = "语音格式编码不合法")
    @Max(value = 3, message = "语音格式编码不合法")
    private Integer formatCode;

    /**
     * 合成语音音量([-10, 10]，如-10表示音量相对默认值小10dB，0表示默认音量，10表示音量相对默认值大10dB)
     */
    @Min(value = -10, message = "音量不合法")
    @Max(value = 10, message = "音量不合法")
    private Integer voiceVolume;

    /**
     * 待合成的文本
     */
    @NotBlank(message = "待合成的文本不能为空")
    @Length(max = 44, message = "最多44个字符")
    private String voiceText;

    /**
     * 合成语音语速(50-200)
     */
    @Min(value = 50, message = "语速不合法")
    @Max(value = 200, message = "语速不合法")
    private Integer voiceSpeed;

    /**
     * 音高(-24, 24)
     */
    @Min(value = -24, message = "音高不合法")
    @Max(value = 24, message = "音高不合法")
    private Integer voiceAht;

    /**
     * 音色(0-100, 默认58)
     */
    @Min(value = 0, message = "音色不合法")
    @Max(value = 100, message = "音色不合法")
    private Integer voiceApc;
}
