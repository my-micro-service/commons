package cn.smile.commons.bean.domain.core;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.FieldFill;
import cn.smile.commons.base.BaseDomain;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author 龙逸
 * @since 2021-06-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("privilege_value")
public class PrivilegeValue extends BaseDomain {

    private static final long serialVersionUID = 1941688209154696354L;
    /**
     * 整型的位
     */
    private Integer position;

    /**
     * 名称
     */
    private String name;

    /**
     * 排序号
     */
    private Integer orderNo;

    /**
     * 备注
     */
    private String remark;
}