package cn.smile.commons.bean.domain.core;

import com.baomidou.mybatisplus.annotation.TableName;

import java.time.LocalDate;

import com.baomidou.mybatisplus.annotation.FieldFill;
import cn.smile.commons.base.BaseDomain;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author 龙逸
 * @since 2021-06-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("privilege_user_group")
public class PrivilegeUserGroup extends BaseDomain {

    private static final long serialVersionUID = 4699497559105064391L;
    /**
     * 用户id
     */
    private Long userId;

    /**
     * 用户工号
     */
    private String userNo;

    /**
     * 角色id
     */
    private Long groupId;

    /**
     * 截止日期
     */
    private LocalDate endDate;

    /**
     * 有效期
     */
    private Integer validMonth;
}