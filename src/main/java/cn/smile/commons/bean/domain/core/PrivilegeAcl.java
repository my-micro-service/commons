package cn.smile.commons.bean.domain.core;

import cn.smile.commons.base.BaseDomain;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigInteger;

/**
 * @author smile-jt
 * @Created 2021/6/28 13:58
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value = "privilege_acl")
public class PrivilegeAcl extends BaseDomain {
    private static final long serialVersionUID = -3482253020731990894L;
    /**
     * 分类id 比方说用户id或者角色id32
     */
    private String releaseId;
    /**
     * 模块id
     */
    private String moduleId;
    /**
     * 模块标示
     */
    private String moduleSn;
    /**
     * acl状态 5
     */
    private BigInteger aclState;
}
