package cn.smile.commons.bean.domain.nowapi;

import com.baomidou.mybatisplus.annotation.TableName;

import cn.smile.commons.base.BaseDomain;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * NowAPI城市列表
 * </p>
 *
 * @author 龙逸
 * @since 2020-11-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("now_api_city")
public class NowApiCity extends BaseDomain {

    private static final long serialVersionUID = 8008150339195957256L;
    /**
     * NowApi编号
     */
    private String weaid;

    /**
     * 城市名
     */
    private String citynm;

    /**
     * 气象ID(与气象网一致)
     */
    private String cityid;

    /**
     * 省
     */
    @TableField("area_1")
    private String area1;

    /**
     * 市
     */
    @TableField("area_2")
    private String area2;

    /**
     * 区
     */
    @TableField("area_3")
    private String area3;

    /**
     * 1:国内  2:国外
     */
    private String coumk;

    /**
     * NowApi更新时间
     */
    private String upddate;

    /**
     * 当前可用状态 1:可用 0:不可用(一般被放弃更新)
     */
    private String mk;

    /**
     * 备注
     */
    private String remark;
}