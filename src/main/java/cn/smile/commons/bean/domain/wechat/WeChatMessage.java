package cn.smile.commons.bean.domain.wechat;

import com.baomidou.mybatisplus.annotation.TableName;

import java.time.LocalDateTime;

import cn.smile.commons.base.BaseDomain;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 接收微信消息表
 * </p>
 *
 * @author 龙逸
 * @since 2020-10-31
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("we_chat_message")
public class WeChatMessage extends BaseDomain {

    private static final long serialVersionUID = 8050117130211624948L;

    /**
     * 接收方微信号
     */
    private String toUserName;

    /**
     * 发送方微信号
     */
    private String fromUserName;

    /**
     * 消息创建时间
     */
    private LocalDateTime msgCreateTime;

    /**
     * 消息类型
     */
    private String msgType;

    /**
     * 事件类型
     */
    private String event;

    /**
     * 事件KEY值
     */
    private String eventKey;

    /**
     * 文本消息内容
     */
    private String content;

    /**
     * 图片消息链接地址
     */
    private String picUrl;

    /**
     * 媒体消息ID
     */
    private String mediaId;

    /**
     * 语音消息格式
     */
    private String format;

    /**
     * 语音消息识别结果
     */
    private String recognition;

    /**
     * 视频消息缩略图ID
     */
    private String thumbMediaId;

    /**
     * 位置消息纬度
     */
    @TableField("location_X")
    private Double locationX;

    /**
     * 位置消息经度
     */
    @TableField("location_Y")
    private Double locationY;

    /**
     * 地图缩放大小
     */
    private Long scale;

    /**
     * 地理位置信息
     */
    private String label;

    /**
     * 链接消息标题
     */
    private String title;

    /**
     * 链接消息描述
     */
    private String description;

    /**
     * 链接消息URL
     */
    private String url;

    /**
     * 消息ID(微信给的)
     */
    private Long msgId;
}