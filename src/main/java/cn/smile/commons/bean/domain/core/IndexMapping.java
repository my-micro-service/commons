package cn.smile.commons.bean.domain.core;

import com.baomidou.mybatisplus.annotation.TableName;
import cn.smile.commons.base.BaseDomain;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 首页映射表
 * </p>
 *
 * @author 龙逸
 * @since 2021-02-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("index_mapping")
public class IndexMapping extends BaseDomain {

    private static final long serialVersionUID = -526806969368531444L;
    /**
     * 域名
     */
    private String url;

    /**
     * IP地址
     */
    private String ipAddress;

    /**
     * 端口
     */
    private Integer portAddress;

    /**
     * 所属公司ID
     */
    private Long companyId;

    /**
     * 跳转页面地址
     */
    private String pageAddress;
}