package cn.smile.commons.bean.domain.core;

import cn.smile.commons.base.BaseDomain;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author 龙逸
 * @since 2020-08-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("core_user")
public class CoreUser extends BaseDomain {


    private static final long serialVersionUID = -6213371476699554036L;

    /**
     * 登录名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 网址
     */
    private String url;

    /**
     * 用户状态：1(已启用) 0(已禁用)
     */
    private Integer status;

    /**
     * 激活码
     */
    private String activationKey;

    /**
     * 用户类型: 1(管理员) 2(普通用户)
     */
    private Integer userType;
}