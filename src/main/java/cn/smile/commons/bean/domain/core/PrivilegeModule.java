package cn.smile.commons.bean.domain.core;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.FieldFill;
import cn.smile.commons.base.BaseDomain;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author 龙逸
 * @since 2021-06-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("privilege_module")
public class PrivilegeModule extends BaseDomain {

    private static final long serialVersionUID = -3925658627417165856L;
    /**
     * 名称
     */
    private String name;

    /**
     * 链接
     */
    private String url;

    /**
     * 标识
     */
    private String sn;

    /**
     * 组件
     */
    private String component;

    /**
     * 左侧菜单是否显示（1：显示，0：不显示）
     */
    private Integer showStatus;

    /**
     * 是否启用 ： 1启用；0禁用
     */
    private Integer status;

    /**
     * 存放该模块有哪些权限值可选
     */
    private String state;

    /**
     * 图片路径
     */
    private String image;

    /**
     * 模块的排序号
     */
    private Integer orderNo;

    /**
     * 父模块id
     */
    private String pid;

    /**
     * 系统id
     */
    private String appId;

    private Integer isShow;
}