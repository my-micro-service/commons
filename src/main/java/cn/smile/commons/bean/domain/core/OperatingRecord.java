package cn.smile.commons.bean.domain.core;

import com.baomidou.mybatisplus.annotation.TableName;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.FieldFill;
import cn.smile.commons.base.BaseDomain;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author 龙逸
 * @since 2021-06-29
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@Builder
@TableName("operating_record")
public class OperatingRecord extends BaseDomain {
    private static final long serialVersionUID = 3224318423956751200L;
    /**
     * 用户登录IP
     */
    private String ip;

    /**
     * 用户编码
     */
    private String userCode;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 操作
     */
    private String operatingContent;

    /**
     * 请求类型
     */
    private String operatingType;

    private String source;

    /**
     * 操作时间
     */
    private LocalDateTime dateTime;
}