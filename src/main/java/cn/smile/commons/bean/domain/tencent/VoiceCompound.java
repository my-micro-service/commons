package cn.smile.commons.bean.domain.tencent;

import com.baomidou.mybatisplus.annotation.TableName;
import cn.smile.commons.base.BaseDomain;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 腾讯AI语音合成接口
 * </p>
 *
 * @author 龙逸
 * @since 2020-12-20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("voice_compound")
public class VoiceCompound extends BaseDomain {

    private static final long serialVersionUID = 8013926304656173822L;
    /**
     * 请求用户
     */
    private String requestUser;

    /**
     * AI接口请求参数Json
     */
    private String requestJson;

    /**
     * 发声人编码(1-普通话男声；5-静琪女声；6-欢馨女声；7-碧萱女声)
     */
    private Integer speakerCode;

    /**
     * 合成语音格式编码(1-PCM；2-WAV；3-MP3)
     */
    private Integer formatCode;

    /**
     * 合成语音音量([-10, 10]，如-10表示音量相对默认值小10dB，0表示默认音量，10表示音量相对默认值大10dB)
     */
    private Integer voiceVolume;

    /**
     * 带合成的文本
     */
    private String voiceText;

    /**
     * 合成语音语速(50-200)
     */
    private Integer voiceSpeed;

    /**
     * 音高(-24, 24)
     */
    private Integer voiceAht;

    /**
     * 音色(0-100, 默认58)
     */
    private Integer voiceApc;

    /**
     * 返回结果Json
     */
    private String resultJson;
}