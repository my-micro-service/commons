package cn.smile.commons.bean.domain.core;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.FieldFill;
import cn.smile.commons.base.BaseDomain;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author 龙逸
 * @since 2021-06-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("privilege_group")
public class PrivilegeGroup extends BaseDomain {

    private static final long serialVersionUID = -861181258280754116L;
    /**
     * 名称
     */
    private String name;

    /**
     * 标识
     */
    private String sn;

    /**
     * 角色等级【数据字典获取】
     */
    private String roleLevel;

    /**
     * 角色所属系统
     */
    private String systemId;

    /**
     * 备注
     */
    private String note;

    /**
     * 有效状态（1：有效；0：失效）
     */
    private Integer validState;
}