package cn.smile.commons.bean.domain.nowapi;

import com.baomidou.mybatisplus.annotation.TableName;
import cn.smile.commons.base.BaseDomain;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * NowApi的天气表
 * </p>
 *
 * @author 龙逸
 * @since 2020-11-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("now_api_weather")
public class NowApiWeather extends BaseDomain {

    private static final long serialVersionUID = -110151118011971330L;
    /**
     * 天气ID
     */
    private String weaid;

    /**
     * 日期
     */
    private String days;

    private String week;

    /**
     * 城市拼音
     */
    private String cityno;

    /**
     * 城市名称
     */
    private String citynm;

    /**
     * 城市ID
     */
    private String cityid;

    /**
     * 当日温度区间
     */
    private String temperature;

    /**
     * 当前温度
     */
    private String temperatureCurr;

    /**
     * 湿度
     */
    private String humidity;

    /**
     * pm2.5
     */
    private String aqi;

    /**
     * 天气(文字)
     */
    private String weather;

    /**
     * 气象图标(原始url)
     */
    private String weatherIcon;

    /**
     * 风向(文字)
     */
    private String wind;

    /**
     * 风力(文字)
     */
    private String winp;

    /**
     * 最高温度
     */
    private String tempHigh;

    /**
     * 最低温度
     */
    private String tempLow;

    /**
     * 最大湿度
     */
    private String humiHigh;

    /**
     * 最小湿度
     */
    private String humiLow;

    /**
     * 天气ID(图片)
     */
    private String weatid;

    /**
     * 风向ID
     */
    private String windid;

    /**
     * 风力ID
     */
    private String winpid;

    /**
     * 气象图标编号
     */
    private String weatherIconid;
}