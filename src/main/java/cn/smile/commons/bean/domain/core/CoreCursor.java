package cn.smile.commons.bean.domain.core;

import com.baomidou.mybatisplus.annotation.TableName;
import cn.smile.commons.base.BaseDomain;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * MyBatis流式查询Demo
 * </p>
 *
 * @author 龙逸
 * @since 2020-12-31
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("core_cursor")
public class CoreCursor extends BaseDomain {


    private static final long serialVersionUID = 1438053540741093954L;
    /**
     * 内容
     */
    private String text;
}