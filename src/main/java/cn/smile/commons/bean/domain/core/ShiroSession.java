package cn.smile.commons.bean.domain.core;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.FieldFill;
import cn.smile.commons.base.BaseDomain;

import java.sql.Blob;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author 龙逸
 * @since 2021-06-29
 */
@SuppressWarnings("SpellCheckingInspection")
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("shiro_session")
public class ShiroSession extends BaseDomain {

    private static final long serialVersionUID = 4686216919795244310L;
    private String sessionId;
    private String session;

    public ShiroSession(String sessionId, String session){
        this.sessionId = sessionId;
        this.session = session;
    }
}