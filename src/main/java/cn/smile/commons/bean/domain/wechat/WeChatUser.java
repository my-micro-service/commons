package cn.smile.commons.bean.domain.wechat;

import cn.smile.commons.base.BaseDomain;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 微信关注用户
 * </p>
 *
 * @author longjuntao
 * @since 2020/11/23 10:02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("we_chat_user")
public class WeChatUser extends BaseDomain {
    private static final long serialVersionUID = 951140864728241378L;

    /**
     * 微信号(系统号,非微信登陆账号)
     */
    private String weChatNumber;

    /**
     * 备注名称
     */
    private String remarkName;

    /**
     * 城市
     */
    private String city;

    /**
     * 气象ID(与气象网一致)
     */
    private String cityId;

    /**
     * 订阅状态：1(订阅) 0(未订阅)
     */
    @TableField(value = "subscribe")
    private Boolean subscribe;

    /**
     * 闲聊API回复类型(1-文本；2-语音)
     */
    private Integer chatBotResultType;
}
